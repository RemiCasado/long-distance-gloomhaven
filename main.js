// main.js
  
requirejs.config({
    baseUrl: 'lib',
    paths: {
        'App':'./app',
    }
});

requirejs(['App'], function(GH) {
    document._GloomHaven = new GH.GloomHaven();
    document._GloomHaven.start();
    setTimeout(()=>{
        console.log(document._GloomHaven);
        document._GloomHaven.start(GH.GloomHaven.GAME_LEVEL_EDITOR);
    }, 1000);
});