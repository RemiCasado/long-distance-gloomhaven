var rotation = 0;
var healthTrackerMaxElem = null;

function yolo(up){
  (up) ? rotation += 12 : rotation -= 12;
  document.getElementById('health-tracker').style.transform = "rotate("+rotation+"deg)";
}

function setAsMax(elem){
  if(healthTrackerMaxElem !== null) healthTrackerMaxElem.classList.remove("health-tracker-max");
  elem.classList.add("health-tracker-max");
  healthTrackerMaxElem = elem;
}

function onMonsterTileEnter(elem){
  document.getElementById('veil').classList.remove('d-none');
  document.getElementById('monster-tile-overview').classList.remove('d-none');
  setTimeout(() => {
    document.getElementById('monster-tile-overview').classList.remove('opacity-0');
  }, 0.25);
  elem.classList.add('z-index-foreground');
}

function onMonsterTileLeave(elem){
  document.getElementById('veil').classList.add('d-none');
  document.getElementById('monster-tile-overview').classList.add('d-none', 'opacity-0');
  elem.classList.remove('z-index-foreground');
}

function onCardSpellEnter(){
  document.getElementById('card-spell-overlay').classList.remove('d-none');
  setTimeout(() => {
    document.getElementById('card-spell-overlay-overview').classList.remove('opacity-0');
  }, 0.25);
}

function onCardSpellLeave(){
  document.getElementById('card-spell-overlay').classList.add('d-none');
  document.getElementById('card-spell-overlay-overview').classList.add('opacity-0');
}