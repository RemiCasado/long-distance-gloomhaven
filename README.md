# Long-Distance-GloomHaven

An online gloomhaven version, with nothing but the assets, for split crews.

Lots of assets here (images) are the property of [Gloomhaven] (http://www.cephalofair.com/gloomhaven) by [Cephalofair Games] (http://www.cephalofair.com/) [Developer: Isaac Childres].