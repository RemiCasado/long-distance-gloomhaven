import { Character } from "../data/character";

export class Player{
    private _id   : number = null;
    private _hero : object = null;
    private _name : string = null;

    public constructor(){};

    public setHero(character_name : string){
        this._hero = Character.getCharacter(character_name);
    }
}