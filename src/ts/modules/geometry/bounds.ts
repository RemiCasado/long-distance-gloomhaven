export class Bounds {

    public w: number = 0;
    public h: number = 0;

    constructor(w: number = 0, h: number = 0) {
        this.w = w;
        this.h = h;
    }

    public get CSSWidth(): string{
        return this.w.toString() + 'px';
    }

    public get CSSHeight(): string{
        return this.h.toString() + 'px';
    }
}