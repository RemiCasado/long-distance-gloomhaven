import { Point } from "./point";

export class Coord {

    public x: number = 0;
    public y: number = 0;
    public z: number = 0;

    private q: number = 0;
    private r: number = 0;

    constructor(x: number = 0, y: number = 0, z: number = 0) {
        this.x = x;
        this.y = y;
        this.z = z;

        this._cubeToAxial();
    }

    public static copy(c: Coord){
        return new Coord(c.x, c.y, c.z);
    }

    public static diff(source: Coord, target: Coord) :Coord{
        return new Coord(source.x - target.x, source.y - target.y, source.z - target.z);
    }

    public static sum(source: Coord, target: Coord) :Coord{
        return new Coord(source.x + target.x, source.y + target.y, source.z + target.z);
    }

    public static tableToCube(col :number, row :number) :Coord{
        var x = col
        var z = row - (col + (col&1)) / 2
        var y = -x-z
        return new Coord(x, y, z);
    }
    
    public fromPoint(point: Point, center: Point, size: number){
        point.x = point.x - center.x;
        point.y = point.y - center.y;
        this.q = ( 2/3 * point.x                            ) / size;
        this.r = (-1/3 * point.x +  Math.sqrt(3)/3 * point.y) / size;

        this._axialToCube();
        this.round();
    }

    public toString(){
        return '( ' + this.x + ', ' + this.y + ', ' + this.z + ' )';
    }

    public round(){
        var rx = Math.round(this.x)
        var ry = Math.round(this.y)
        var rz = Math.round(this.z)

        var x_diff = Math.abs(rx - this.x)
        var y_diff = Math.abs(ry - this.y)
        var z_diff = Math.abs(rz - this.z)

        if(x_diff > y_diff && x_diff > z_diff){
            rx = -ry-rz;
        }
        else if(y_diff > z_diff){
            ry = -rx-rz;
        }
        else{
            rz = -rx-ry;
        }
        this.x = (rx === -0) ? 0 : rx;
        this.y = (ry === -0) ? 0 : ry;
        this.z = (rz === -0) ? 0 : rz;

        this._cubeToAxial();
    }

    public rotation(tics :number, center: Coord){
        let centeredCoord = new Coord(this.x - center.x, this.y - center.y, this.z - center.z);
        for(let i = 0; i < tics; i++){
            centeredCoord = new Coord(-centeredCoord.z, -centeredCoord.x, -centeredCoord.y);
        }
        this.x = centeredCoord.x + center.x;
        this.y = centeredCoord.y + center.y; 
        this.z = centeredCoord.z + center.z;
        this._cubeToAxial();
    }

    public toPoint(size: number, center: Point): Point{
        return this.axialToPixel(size, center);
    }

    public isEqual(target: Coord): boolean{
        if(target === null) return false;
        return (target.x === this.x && target.y === this.y && target.z === this.z);
    }

    public axialToPixel(size: number = 0, center: Point) {
        let x = size * ( 3/2 * this.q ) + center.x;
        let y = size * (Math.sqrt(3)/2 * this.q  +  Math.sqrt(3) * this.r) + center.y;
        return new Point(x, y);
    }

    private _cubeToAxial(): void{
        this.q = this.x;
        this.r = this.z;
    }

    private _axialToCube(): void{
        this.x = this.q;
        this.z = this.r;
        this.y = -this.x-this.z;
    }

    public static getRelativeCoord(origin: Coord, target: Coord): Coord{
        return new Coord(target.x + origin.x, target.y + origin.y, target.z + origin.z);
    }


    /**
     * Translate the coord with a delta.
     *
     * @param {Coord} delta
     * @memberof Coord
     */
    public translate(delta: Coord) :void{
        this.x += delta.x;
        this.y += delta.y;
        this.z += delta.z;
        this._cubeToAxial();
    }
}

