
export class Point {

    public x: number = 0;
    public y: number = 0;

    constructor(x: number = 0, y: number = 0) {
        this.x = x;
        this.y = y;
    }

    public get CSSX(): string{
        return this.x.toString() + 'px';
    }

    public get CSSY(): string{
        return this.y.toString() + 'px';
    }
}