import {Point}  from "../geometry/point";
import {Bounds} from "../geometry/bounds";
import { Coord } from "../geometry/coord";

export class Tile {

    public element    :HTMLElement   = null;

    private _overlay  :HTMLCanvasElement = null;
    private _coord    :Coord             = null;
    private _bounds   :Bounds            = null;
    private _center   :Point             = null;
    private _textures :Array<string>     = [];
    private _size     :number            = null;

    constructor(coord :Coord = null, style :Array<string> = null, size: number = 0, center: Point = new Point(0, 0)) {
        this._size    = size;
        this._center  = center;
        this._textures  = style;
        this._coord   = coord;
        this.element  = document.createElement('div');
        this._overlay = document.createElement('canvas');

        this._overlay.classList.add('tile-overlay', 'd-none');
        this.element.appendChild(this._overlay);

        this.element.classList.add('tile');
        this._textures.forEach(style => {
            this.element.classList.add(style);
        });
        this.updateElement();
    }

    public showOverLay(path: Array<boolean>) :void{
        // Faire le tour en regardant les voisins.
        let moveto :boolean       = true;
        let w      :number        = Math.round(this._bounds.w);
        let h      :number        = Math.round(this._bounds.h);
        let pos    :Array<number> = [2        , h / 2, 
                                     w / 4    ,     2, 
                                     w * 3 / 4,     2,
                                     w        , h / 2,
                                     w * 3 / 4, h - 2,
                                     w / 4    , h - 2,
                                     2        , h / 2]

        this._overlay.width  = w;
        this._overlay.height = h;
        this._overlay.classList.add("overlay-green");

        var ctx = this._overlay.getContext("2d");
        ctx.beginPath();
        ctx.lineJoin = "bevel";
        ctx.lineCap = "round";
        ctx.strokeStyle = 'rgb(0,255,0';
        ctx.lineWidth = 4;

        for(let i = 0 ; i < path.length; i++){
            if(path[i]){
                if(moveto){
                    ctx.moveTo(pos[i * 2]    , pos[i * 2 + 1]);
                    ctx.lineTo(pos[(i+1) * 2], pos[(i+1) * 2 + 1]);
                    moveto = false;
                }
                else{
                    ctx.lineTo(pos[(i+1) * 2], pos[(i+1) * 2 + 1]);
                }
            }
            else{
                moveto = true;
            }
        }
        ctx.stroke(); 
        this._overlay.classList.remove('d-none');
    }

    public static copy(t: Tile){
        return new Tile(t._coord, t._textures, t._size, t._center);
    }

    public updateCoord(coord :Coord){
        this._coord = coord;
    }

    public translateCoord(coord: Coord){
        Coord.sum(this._coord, coord);
    }

    public getCoord() :Coord{
        return this._coord;
    }

    public getBounds(): Bounds {
        return this._bounds;
    }

    public setCenter(p: Point): void {
        this._center = p;
    }

    public setSize(s: number): void{
        this._size = s;
    }

    public updateElement(): void {
        this._bounds = new Bounds(this._size * 2, this._size * Math.sqrt(3));
        this.element.style.height   = this._bounds.CSSHeight;
        this.element.style.width    = this._bounds.CSSWidth;
        this.element.style.top      = (this._center.y - this._bounds.h / 2).toString() + 'px';
        this.element.style.left     = (this._center.x - this._bounds.w / 2).toString() + 'px';
    }

    public setTexture(textures :Array<string>) :void{
        this._textures.forEach((texture) => {
            this.element.classList.remove(texture);
        });
        this._textures = textures;
        this._textures.forEach((texture) => {
            this.element.classList.add(texture);
        });
    }
    
}