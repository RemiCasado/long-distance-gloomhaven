import { Layer } from "./layer";
import { Point } from "../geometry/point";
import { Coord } from "../geometry/coord";
import { Tileset } from "./tileset";
import { DomUtils } from "../utils/dom";

export class MouseMoveLayer extends Layer{

    private _active            :boolean       = false;
    private _center            :Point         = new Point(0, 0);
    private _size              :number        = 0;
    private _tileSet           :Array<object> = null;
    private _tileSetCoord      :Coord         = null;

    constructor(element: HTMLElement){
        super(element);
    }

    public setBounds(center :Point, size :number){
        this._center = center;
        this._size = size;
    }

    public startHoverTileSet(tileSet :Array<object>, coord :Coord){
        this._tileSet = tileSet;
        this._tileSetCoord = coord;
        this._active  = true;
    }

    public stopHoverTileSet(){
        this._active  = false;
        return this._tileSet;
    }

    public onMouseMove(evt) :object{
        let result = null;
        if(this._active){
            let rect = DomUtils.elementOffset(this._element);
            let x = evt.clientX - rect['left'];
            let y = evt.clientY - rect['top'];

            // Calculate target coord.
            let mouse_coord = new Coord();
            mouse_coord.fromPoint(new Point(x, y), this._center, this._size);

            // Move if necessary.
            if(!mouse_coord.isEqual(this._tileSetCoord)){
                let hover_tileset = Tileset.copy(this._tileSet);
                result = {
                    'hover_tileset': hover_tileset,
                    'mouse_coord': mouse_coord,
                    'hover_tileset_coord': Coord.copy(this._tileSetCoord)
                }
                this._tileSetCoord = mouse_coord;
            }
        }
        return result;
    }

}