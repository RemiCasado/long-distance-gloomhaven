import { Coord }   from "../geometry/coord";
import { Tile }    from "./tile";
import { Tileset } from "./tileset";
import { Point }   from "../geometry/point";

export class Layer{

    protected _element    :HTMLElement          = null;
    protected _size       :number               = null;
    protected _center     :Point                = null;
    protected _tilesets   :Map<symbol, Tileset> = new Map<symbol, Tileset>();
    protected _background :string               = null;

    constructor(element: HTMLElement){
        this._element = element;
    }

    public setCenter(center :Point){
        this._center = center;
    }

    public setSize(size: number){
        this._size = size;
    }

    public setBackground(background :string) :void{
        if(this._background !== null) this._element.classList.remove(this._background);
        this._background = background;
        this._element.classList.add(this._background);
    }

    public addTileset(tileset: Tileset): void{
        tileset.resizeTiles(this._size);
        tileset.translateTiles(null, this._size, this._center);
        tileset.getTilesAsArray().forEach((tile) => {
            this._element.appendChild(tile.element);
        });
        this._tilesets.set(tileset.getId(), tileset);
    }

    public updateTileset(id: symbol) :void{
        let tileset = this._tilesets.get(id);
        tileset.getTilesAsArray().forEach((tile) => {
            tile.setCenter(tile.getCoord().toPoint(this._size, this._center));
            tile.updateElement();
        });
    }

    public getTileset(id: symbol) :Tileset{
        return this._tilesets.get(id);
    }

    public moveTileset(id: symbol) :void{
        let tileset = this._tilesets.get(id);
        tileset.getTilesAsArray().forEach((tile) => {
            this._element.removeChild(tile.element);
        });
        this._tilesets.delete(id);
    }

    public deleteTileset(id :symbol) :void{
        let tileset = this._tilesets.get(id);
        tileset.getTilesAsArray().forEach((tile) => {
            this._element.removeChild(tile.element);
        });
        this._tilesets.delete(id);
    }
}