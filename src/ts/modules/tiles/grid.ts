import {Point}   from "../geometry/point";
import {Tile}    from "./tile";
import {Coord}   from "../geometry/coord";
import {Tileset} from "./tileset";
import { Layer } from "./layer";
import { MouseMoveLayer } from "./mouseMoveLayer";

export class Grid {

    public width:        number        = 0;
    public height:       number        = 0;
    public size:         number        = 0;
    public element:      HTMLElement   = null;
    
    private center:               Point   = new Point(0, 0);
    private _layers              :Map<number, Layer> = new Map<number, Layer>();

    constructor(w: number = 0, h: number = 0, s: number = 0, elem: HTMLElement) {
        this.element = elem;
        this.width   = w;
        this.height  = h;
        this.size    = s;
        this.center  = new Point(this.width / 2, this.height / 2);

        // this.element.addEventListener('mousemove', this._onMouseMove.bind(this)); 
    }

    public setLayer(index: number, layer: Layer){
        layer.setCenter(this.center);
        layer.setSize(this.size);
        this._layers.set(index, layer);
    }

    public deleteLayer(index: number){
        this._layers.delete(index);
    }

    public setBounds(width :number, height :number){
        this.width   = width;
        this.height  = height;
        this.center  = new Point(this.width / 2, this.height / 2);

        this._layers.forEach((layer, layerIndex) => {
            layer.setCenter(this.center);
            layer.setSize(this.size);
        })
    }

    // public displayBackgroundGrid(indexLayer :number) :void{
    //     let xLength :number = Math.round(this.width  / (3 * this.size /2));
    //     if(xLength%2 !== 0) xLength += 1;
    //     let yLength :number = Math.round(this.height / (Math.sqrt(3) * this.size));
    //     if(yLength%2 !== 0) yLength += 1;

    //     for(let i = 0 ; i <= xLength + 1; i++){
    //         for(let j = 0; j <= yLength + 1; j++){
    //             let x :number = i - xLength / 2;
    //             let y :number = j - yLength / 2;
    //             let c :Coord = Coord.tableToCube(x, y);
    //             this.addTile(indexLayer, c, new Tile(['tile-background']));
    //         }
    //     }
    // }

    // public addTile(layerIndex: number, coord: Coord, tile: Tile, verbose = false): void{
    //     if(tile === undefined || tile === null) throw "Tile is not set."; //TODO : add more details.

    //     tile.setSize(this.size);
    //     tile.setCenter(coord.toPoint(this.size, this.center));
    //     tile.updateElement();

    //     if(verbose){
    //         tile.element.innerHTML = `<div>${coord.x.toString()}</div><div>${coord.y.toString()}</div><div>${coord.z.toString()}</div>`; 
    //     }

    //     this._layers.get(layerIndex).addTile(coord, tile);
    // }

    public addTileSet(layerIndex :number, tileset :Tileset): void{
        this._layers.get(layerIndex).addTileset(tileset);
    }

    // public getTile(layerIndex: number, coord: Coord): Tile{
    //     return this._layers.get(layerIndex).getTile(coord);
    // }

    // public removeTileSet(layerIndex: number, tileset: Array<object>, center: Coord = null){
    //     for(let i = 0; i < tileset.length; i++){
    //         let coord = tileset[i]['c'];
    //         if(center !== null) coord = Coord.getRelativeCoord(center, coord);
    //         let tile = this._layers.get(layerIndex).getTile(coord);
    //         if(tile !== null){
    //             this._layers.get(layerIndex).addTile(coord, null);
    //         }
    //     }
    //     return tileset;
    // }

    // public moveTileSet(layerIndex: number, tileset: Array<object>, target: Coord, source: Coord = null): Array<object>{
    //     tileset = this.removeTileSet(layerIndex, tileset, source);
    //     return this.addTileSet(layerIndex, tileset, target);
    // }

    // private _onMouseMove(evt): void{
    //     this._layers.forEach((layer, layerIndex) => {
    //         if(layer instanceof MouseMoveLayer){
    //             let coords = layer.onMouseMove(evt);
    //             if(coords !== null) this.moveTileSet(layerIndex, coords['hover_tileset'], coords['mouse_coord'], coords['hover_tileset_coord']);
    //         }
    //     })
    // }

    // public startHoverTileset(layerIndex: number, tileSet: Array<object>): void{
    //     let coord = new Coord(0, 0, 0);
    //     tileSet = this.addTileSet(layerIndex, tileSet, coord);
    //     let layer :MouseMoveLayer = <MouseMoveLayer> this._layers.get(layerIndex);
    //     layer.startHoverTileSet(tileSet, coord);
    // }

    // public stopHoverTileset(layerIndex: number): void{
    //     let layer :MouseMoveLayer = <MouseMoveLayer> this._layers.get(layerIndex);
    //     let tileSet = layer.stopHoverTileSet();
    //     this.removeTileSet(layerIndex, tileSet);
    // }


}