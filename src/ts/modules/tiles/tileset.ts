import { Coord } from "../geometry/coord";
import { Tile }  from "./tile";
import { Point } from "../geometry/point";

export class Tileset {

    
    private _id            :symbol      = null;
    private _tiles         :Array<Tile> = null;

    public constructor(tiles :Array<Tile>){
        this._id = Symbol();
        this._tiles = new Array<Tile>();

        for(let i = 0; i < tiles.length; i++){
            this._tiles.push(tiles[i]);
        }
    }

    public getId(): symbol{
        return this._id;
    }

    public getTilesAsArray(): Array<Tile>{
        return this._tiles;
    }

    public showOverlay() :void{
        for(let i = 0; i < this._tiles.length; i++){
            let path = [true, true, true, true, true, true];
            let tile = this._tiles[i];
            for(let j = 0; j < this._tiles.length; j++){
                if(i !== j){
                    let tileB = this._tiles[j];
                    let coordA = tile.getCoord();
                    let coordB = tileB.getCoord();
                    if(coordB.x === coordA.x - 1 && coordB.y === coordA.y + 1 && coordB.z === coordA.z)     path[0] = false;
                    if(coordB.x === coordA.x + 1 && coordB.y === coordA.y - 1 && coordB.z === coordA.z)     path[3] = false;
                    if(coordB.x === coordA.x     && coordB.y === coordA.y - 1 && coordB.z === coordA.z + 1) path[4] = false;
                    if(coordB.x === coordA.x     && coordB.y === coordA.y + 1 && coordB.z === coordA.z - 1) path[1] = false;
                    if(coordB.x === coordA.x + 1 && coordB.y === coordA.y     && coordB.z === coordA.z - 1) path[2] = false;
                    if(coordB.x === coordA.x - 1 && coordB.y === coordA.y     && coordB.z === coordA.z + 1) path[5] = false;
                }
            }
            tile.showOverLay(path);
        }
    }

    public rotate(tics :number, center :Coord = new Coord(0, 0, 0)){
        for(let i = 0; i < this._tiles.length; i++){
            this._tiles[i].getCoord().rotation(tics, center);
        }
    }

    public resizeTiles(size: number) :void{
        this._tiles.forEach((tile) => {
            tile.setSize(size);
            tile.updateElement();
        });
    }

    public translateTiles(coord :Coord, size :number, center :Point): void{
        if(coord === null) coord = this._tiles[0].getCoord();
        let diff = Coord.diff(this._tiles[0].getCoord(), coord);
        this._tiles.forEach((tile) => {
            tile.translateCoord(diff);
            tile.setCenter(tile.getCoord().toPoint(size, center));
            tile.updateElement();
        });
    }

    public static copy(tileset: Array<object>){
        let new_tileset = [];
        for(let i = 0 ; i < tileset.length; i++){
            let coord = tileset[i]['c'];
            let tile  = tileset[i]['t'];
            new_tileset.push({
                c: Coord.copy(coord),
                t: Tile.copy(tile)
            })
        }
        return new_tileset;
    }
}