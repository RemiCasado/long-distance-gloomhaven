import { Grid }      from "../tiles/grid";
import { MapTiles } from "../data/mapTiles";
import { Coord }     from "../geometry/coord";
import { Point }     from "../geometry/point";
import { Layer } from "../tiles/layer";
import { MouseMoveLayer } from "../tiles/mouseMoveLayer";
import { Tileset } from "../tiles/tileset";

export class GameLevelEditor {

    // Main HTMLElements
    public static container :HTMLElement = document.getElementById('game-level-editor');
    public static header    :HTMLElement = document.getElementById('game-level-editor-header');
    public static sandbox   :HTMLElement = document.getElementById('game-level-editor-sandbox');
    public static assets    :HTMLElement = document.getElementById('game-level-editor-assets');

    // Sandbox HTMLElements
    public static background :HTMLElement = document.getElementById('game-level-editor-sandbox-background');
    public static map        :HTMLElement = document.getElementById('game-level-editor-sandbox-map');
    public static overlay    :HTMLElement = document.getElementById('game-level-editor-sandbox-overlay');
    public static characters :HTMLElement = document.getElementById('game-level-editor-sandbox-characters');
    public static mouseover  :HTMLElement = document.getElementById('game-level-editor-sandbox-mouseover');

    // Tile Map assets HTMLElements
    public static tilePreview   :HTMLElement       = document.getElementById('game-level-editor-assets-tiles-map-preview');
    public static tileRotatep   :HTMLElement       = document.getElementById('game-level-editor-assets-tiles-map-rotation-p');
    public static tileRotatem   :HTMLElement       = document.getElementById('game-level-editor-assets-tiles-map-rotation-m');
    public static tileFloorG    :HTMLElement       = document.getElementById('game-level-editor-assets-tiles-map-g');
    public static tileFloorNSB  :HTMLElement       = document.getElementById('game-level-editor-assets-tiles-map-nsb');
    public static tileFloorMMS  :HTMLElement       = document.getElementById('game-level-editor-assets-tiles-map-mms');
    public static tileFloorW    :HTMLElement       = document.getElementById('game-level-editor-assets-tiles-map-w');

    //Layers indexes
    public static backgroundLayer  :number = 1;
    public static mapLayer         :number = 2;
    public static overlayLayer     :number = 3;
    public static charactersLayer  :number = 4;
    public static mouseoverLayer   :number = 5;


    private _tileSize :number        = 50;
    private _grid :Grid              = null;

    private _tilePreviewLayer           :Layer              = null;
    private _tilePreviewSize            :number             = 7;
    private _tilePreviewTilemapTilesets :Map<symbol, Layer> = null;

    constructor(){
        this._grid = new Grid(0, 0, this._tileSize, GameLevelEditor.sandbox);
        this._grid.setLayer(GameLevelEditor.backgroundLayer, new Layer(GameLevelEditor.background));
        this._grid.setLayer(GameLevelEditor.mapLayer,        new Layer(GameLevelEditor.map));
        this._grid.setLayer(GameLevelEditor.overlayLayer,    new Layer(GameLevelEditor.overlay));
        this._grid.setLayer(GameLevelEditor.charactersLayer, new Layer(GameLevelEditor.characters));

        this._tilePreviewLayer = new Layer(GameLevelEditor.tilePreview);
        this._tilePreviewLayer.setSize(this._tilePreviewSize);

        GameLevelEditor.tileRotatep.addEventListener('click', () => {this._rotateTileMapPreview(true);});
        GameLevelEditor.tileRotatem.addEventListener('click', () => {this._rotateTileMapPreview(false);});

        GameLevelEditor.tileFloorG.addEventListener('click', () => {
            GameLevelEditor.tileFloorW.classList.add("opacity-25");
            GameLevelEditor.tileFloorNSB.classList.add("opacity-25");
            GameLevelEditor.tileFloorMMS.classList.add("opacity-25");
            GameLevelEditor.tileFloorG.classList.remove("opacity-25");
            this._changeTileFloor(MapTiles.STYLE_G);
        });
        GameLevelEditor.tileFloorW.addEventListener('click', () => {
            GameLevelEditor.tileFloorW.classList.remove("opacity-25");
            GameLevelEditor.tileFloorNSB.classList.add("opacity-25");
            GameLevelEditor.tileFloorMMS.classList.add("opacity-25");
            GameLevelEditor.tileFloorG.classList.add("opacity-25");
            this._changeTileFloor(MapTiles.STYLE_W);
        });
        GameLevelEditor.tileFloorNSB.addEventListener('click', () => {
            GameLevelEditor.tileFloorW.classList.add("opacity-25");
            GameLevelEditor.tileFloorNSB.classList.remove("opacity-25");
            GameLevelEditor.tileFloorMMS.classList.add("opacity-25");
            GameLevelEditor.tileFloorG.classList.add("opacity-25");
            this._changeTileFloor(MapTiles.STYLE_NSB);
        });
        GameLevelEditor.tileFloorMMS.addEventListener('click', () => {
            GameLevelEditor.tileFloorW.classList.add("opacity-25");
            GameLevelEditor.tileFloorNSB.classList.add("opacity-25");
            GameLevelEditor.tileFloorMMS.classList.remove("opacity-25");
            GameLevelEditor.tileFloorG.classList.add("opacity-25");
            this._changeTileFloor(MapTiles.STYLE_MMS);
        });
    }

    public display() :void{
        GameLevelEditor.container.classList.remove('d-none');
        let width: number  = GameLevelEditor.sandbox.offsetWidth;
        let height: number = GameLevelEditor.sandbox.offsetHeight;
        this._grid.setBounds(width, height);
        let center = new Point(GameLevelEditor.tilePreview.offsetWidth / 2, GameLevelEditor.tilePreview.offsetHeight / 2);
        this._tilePreviewLayer.setCenter(center);

        // let that = this;
        // setInterval(()=>{
        //     ts.rotate(1);
        //     that._tilePreviewLayer.updateTileset(ts.getId());
        // }, 1000);

        // setTimeout(()=>{
        //     ts.showOverlay();
        //     console.log('ok');
        // }, 1000);

        // this._grid.displayBackgroundGrid(GameLevelEditor.backgroundLayer);
        // setTimeout(() =>{
        //     let ts = MapTiles.A();
        //     this._grid.startHoverTileset(GameLevelEditor.mouseoverLayer, ts);
        // }, 1000);

        this._tilePreviewTilemapTilesets = new Map<symbol, Layer>();
        let names :Array<string>= MapTiles.names();
        for(let i = 0; i < names.length; i++){
            let parent = document.createElement('div');
            GameLevelEditor.tilePreview.appendChild(parent);
            let div = document.createElement('div');
            parent.appendChild(div);
            let layer = new Layer(div);
            layer.setBackground("greenish-background");
            let center = new Point(div.offsetWidth / 2, div.offsetHeight / 2);
            layer.setSize(this._tilePreviewSize);
            layer.setCenter(center);
            let ts = new Tileset(MapTiles.tile(names[i]));
            layer.addTileset(ts);
            this._tilePreviewTilemapTilesets.set(ts.getId(), layer);
        }
    }

    private _changeTileFloor(style :number){
        this._tilePreviewTilemapTilesets.forEach((layer, id) =>{
            let ts :Tileset = layer.getTileset(id);
            let tiles = ts.getTilesAsArray();
            tiles.forEach((tile) => {
                tile.setTexture(MapTiles.getTexture(style));
            });
            switch(style){
                case MapTiles.STYLE_G:
                    layer.setBackground("greenish-background");
                    break;
                case MapTiles.STYLE_W:
                    layer.setBackground("yellowish-background");
                    break;
                case MapTiles.STYLE_NSB:
                    layer.setBackground("blueish-background");
                    break;
                case MapTiles.STYLE_MMS:
                    layer.setBackground("redish-background");
                    break;
                default:
                    layer.setBackground("greenish-background");
                    break;
            }
            
        });
    }

    private _rotateTileMapPreview(rotateRight :boolean) :void{
        this._tilePreviewTilemapTilesets.forEach((layer, id) =>{
            let ts :Tileset = layer.getTileset(id);
            (rotateRight) ? ts.rotate(1) : ts.rotate(5);
            layer.updateTileset(id);
        });
    }

    public hide() :void{
        GameLevelEditor.container.classList.add('d-none');
    }
}