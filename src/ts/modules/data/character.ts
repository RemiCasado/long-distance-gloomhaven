export class Character{

    public static getCharacter(name: string){
        switch(name){
            case "Tinkerer":
                return this.Tinkerer;
        }
    } 

    private static get Tinkerer() : object{
        return {
            "name":  "Tinkerer",
            "cards": 12,
            "hp":    [8,9,11,12,14,15,17,18,20]
        }
    }
}