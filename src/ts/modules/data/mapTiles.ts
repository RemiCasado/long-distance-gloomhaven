import { Coord } from "../geometry/coord";
import { Tile } from "../tiles/tile";
import { Tileset } from "../tiles/tileset";

export class MapTiles{

    public static STYLE_G   :number = 0;
    public static STYLE_W   :number = 1;
    public static STYLE_NSB :number = 2;
    public static STYLE_MMS :number = 3;

    public static tile(type :string, style :number = MapTiles.STYLE_G) :Array<Tile>{
        switch(type){
            case 'A':
                return MapTiles.A(style);
            case 'B':
                return MapTiles.B(style);
            case 'C':
                return MapTiles.C(style);
            case 'D':
                return MapTiles.D(style);
            case 'E':
                return MapTiles.E(style);
            case 'F':
                return MapTiles.F(style);
            case 'G':
                return MapTiles.G(style);
            case 'H':
                return MapTiles.H(style);
            case 'I':
                return MapTiles.I(style);
            case 'J':
                return MapTiles.J(style);
            case 'K':
                return MapTiles.K(style);
            case 'L':
                return MapTiles.L(style);
            case 'M':
                return MapTiles.M(style);
            case 'N':
                return MapTiles.N(style);
            default:
                return null;
        }
    }

    public static names() : Array<string>{
        return ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N'];
    }

    public static A(style: number = MapTiles.STYLE_G) :Array<Tile>{

        let coords :Array<Coord> = [new Coord( 0, 0, 0), new Coord(1, -1, 0), new Coord(2, -2, 0),
                      new Coord(3, -3, 0), new Coord( -1, 0, 1), new Coord( 0, -1, 1),
                      new Coord( 1,-2, 1),new Coord( 2,-3, 1), new Coord( 3,-4, 1)];
        coords.forEach((coord) => {
            coord.translate(new Coord(-1, +1, -1));
        })
        let tiles :Array<Tile> = [];
        for(let i = 0; i < coords.length; i++){
            tiles.push(new Tile(coords[i], MapTiles.getTexture(style)));
        }
        return tiles;
    }

    public static B(style: number = MapTiles.STYLE_G) :Array<Tile>{

        let coords :Array<Coord> = [new Coord(  0,  0, 0), new Coord(  1, -1, 0), new Coord( 2, -2, 0), new Coord( 3, -3, 0),
                                    new Coord(  0, -1, 1), new Coord(  1, -2, 1), new Coord( 2, -3, 1),
                                    new Coord( -1, -1, 2), new Coord(  0, -2, 2), new Coord( 1, -3, 2), new Coord( 2, -4, 2),
                                    new Coord( -1, -2, 3), new Coord(  0, -3, 3), new Coord( 1, -4, 3)];
        
        // Center the tile
        coords.forEach((coord) => {
            coord.translate(new Coord(-1, +2, -1));
        })

        let tiles :Array<Tile> = [];
        for(let i = 0; i < coords.length; i++){
            tiles.push(new Tile(coords[i], MapTiles.getTexture(style)));
        }
        return tiles;
    }

    public static C(style: number = MapTiles.STYLE_G) :Array<Tile>{

        let coords :Array<Coord> = [new Coord(  0,  0, 0), new Coord(  1, -1, 0),
                                    new Coord( -1,  0, 1), new Coord(  0, -1, 1), new Coord(  1, -2, 1),
                                    new Coord( -2,  0, 2), new Coord( -1, -1, 2), new Coord(  0, -2, 2), new Coord( 1, -3, 2),
                                    new Coord( -2, -1, 3), new Coord( -1, -2, 3), new Coord(  0, -3, 3)];
        let tiles :Array<Tile> = [];

        // Center the tile
        coords.forEach((coord) => {
            coord.translate(new Coord(0, +1, -1));
        })

        for(let i = 0; i < coords.length; i++){
            tiles.push(new Tile(coords[i], MapTiles.getTexture(style)));
        }
        return tiles;
    }

    public static D(style: number = MapTiles.STYLE_G) :Array<Tile>{
        let x :number = 0;
        let y :number = 0;
        let z :number = 0;

        let coords :Array<Coord> = []

        // First line
        for(let i = 0; i < 3; i++){
            coords.push(new Coord(x+i, y-i, z));
        }

        // Second line
        x = -1; y = 0; z = 1;
        for(let i = 0; i < 4; i++){    
            coords.push(new Coord(x+i, y-i, z));
        }

        // Third line
        x = -2; y = 0; z = 2;
        for(let i = 0; i < 5; i++){    
            coords.push(new Coord(x+i, y-i, z));
        }

        // Fourth line
        x = -2; y = -1; z = 3;
        for(let i = 0; i < 4; i++){    
            coords.push(new Coord(x+i, y-i, z));
        }

        // Fifth line
        x = -2; y = -2; z = 4;
        for(let i = 0; i < 3; i++){    
            coords.push(new Coord(x+i, y-i, z));
        }

        // Center the tile
        coords.forEach((coord) => {
            coord.translate(new Coord(0, +2, -2));
        })

        let tiles :Array<Tile> = [];
        for(let i = 0; i < coords.length; i++){
            tiles.push(new Tile(coords[i], MapTiles.getTexture(style)));
        }
        return tiles;
    }
    
    public static E(style: number = MapTiles.STYLE_G) :Array<Tile>{
        let x :number = 0;
        let y :number = 0;
        let z :number = 0;

        let coords :Array<Coord> = []

        // First line
        for(let i = 0; i < 4; i++){
            coords.push(new Coord(x+i, y-i, z));
        }

        // Second line
        x = -1; y = 0; z = 1;
        for(let i = 0; i < 5; i++){    
            coords.push(new Coord(x+i, y-i, z));
        }

        // Third line
        x = -1; y = -1; z = 2;
        for(let i = 0; i < 4; i++){    
            coords.push(new Coord(x+i, y-i, z));
        }

        // Fourth line
        x = -2; y = -1; z = 3;
        for(let i = 0; i < 5; i++){    
            coords.push(new Coord(x+i, y-i, z));
        }

        // Fifth line
        x = -2; y = -2; z = 4;
        for(let i = 0; i < 4; i++){    
            coords.push(new Coord(x+i, y-i, z));
        }
        
        // Center the tile
        coords.forEach((coord) => {
            coord.translate(new Coord(-1, +3, -2));
        })

        let tiles :Array<Tile> = [];
        for(let i = 0; i < coords.length; i++){
            tiles.push(new Tile(coords[i], MapTiles.getTexture(style)));
        }
        return tiles;
    }

    public static F(style: number = MapTiles.STYLE_G) :Array<Tile>{
        let x   :number = 0;
        let y   :number = 0;
        let z   :number = 0;
        let col :number = 3;
        let row :number = 9;

        let coords :Array<Coord> = []

        for(let i = 0; i < row; i++){
            for(let j = 0; j < col; j++){
                coords.push(new Coord(x+j, y-j, z));
            }
            if(i%2 === 0){
                y--;
                col = 2;
            }
            else{
                x--;
                col = 3;
            }
            z++;
        }
        
        // Center the tile
        coords.forEach((coord) => {
            coord.translate(new Coord(+1, +3, -4));
        })

        let tiles :Array<Tile> = [];
        for(let i = 0; i < coords.length; i++){
            tiles.push(new Tile(coords[i], MapTiles.getTexture(style)));
        }
        return tiles;
    }

    public static G(style: number = MapTiles.STYLE_G) :Array<Tile>{
        let x   :number = 0;
        let y   :number = 0;
        let z   :number = 0;
        let col :number = 8;
        let row :number = 3;

        let coords :Array<Coord> = []

        for(let i = 0; i < row; i++){
            for(let j = 0; j < col; j++){
                coords.push(new Coord(x+j, y-j, z));
            }
            if(i%2 === 0){
                y--;
                col = 7;
            }
            else{
                x--;
                col = 8;
            }
            z++;
        }
        
        // Center the tile
        coords.forEach((coord) => {
            coord.translate(new Coord(-3, +4, -1));
        })

        let tiles :Array<Tile> = [];
        for(let i = 0; i < coords.length; i++){
            tiles.push(new Tile(coords[i], MapTiles.getTexture(style)));
        }
        return tiles;
    }

    public static H(style: number = MapTiles.STYLE_G) :Array<Tile>{
        let x   :number = 0;
        let y   :number = 0;
        let z   :number = 0;
        let col :number = 6;
        let row :number = 2;

        let coords :Array<Coord> = []

        for(let i = 0; i < row; i++){
            for(let j = 0; j < col; j++){
                coords.push(new Coord(x+j, y-j, z));
            }
            x--;
            col = 7;
            z++;
        }

        col =  2;
        row =  5;
        x   =  1;
        y   = -3;

        for(let i = 0; i < row; i++){
            for(let j = 0; j < col; j++){
                coords.push(new Coord(x+j, y-j, z));
            }
            if(i%2 === 1){
                y--;
                col = 2;
            }
            else{
                x--;
                col = 3;
            }
            z++;
        }

        // Center the tile
        coords.forEach((coord) => {
            coord.translate(new Coord(-1, +4, -3));
        })
        
        let tiles :Array<Tile> = [];
        for(let i = 0; i < coords.length; i++){
            tiles.push(new Tile(coords[i], MapTiles.getTexture(style)));
        }
        return tiles;
    }

    public static I(style: number = MapTiles.STYLE_G) :Array<Tile>{
        let x   :number = 0;
        let y   :number = 0;
        let z   :number = 0;
        let col :number = 6;
        let row :number = 5;

        let coords :Array<Coord> = []

        for(let i = 0; i < row; i++){
            for(let j = 0; j < col; j++){
                coords.push(new Coord(x+j, y-j, z));
            }
            if(i%2 === 0){
                y--;
                col = 5;
            }
            else{
                x--;
                col = 6;
            }
            z++;
        }

        // Center the tile
        coords.forEach((coord) => {
            coord.translate(new Coord(-2, +5, -2));
        })

        let tiles :Array<Tile> = [];
        for(let i = 0; i < coords.length; i++){
            tiles.push(new Tile(coords[i], MapTiles.getTexture(style)));
        }
        return tiles;
    }

    public static J(style: number = MapTiles.STYLE_G) :Array<Tile>{
        let x   :number = 0;
        let y   :number = 0;
        let z   :number = 0;
        let col :number = 3;
        let row :number = 6;

        let coords :Array<Coord> = [new Coord(0, 0, 0)];

        x = -2;
        y =  1;
        z =  1;

        for(let i = 0; i < row; i++){
            for(let j = 0; j < col; j++){
                coords.push(new Coord(x+j, y-j, z));
            }
            y--;
            z++;
            if(i === 2) col = 7;
            if(i === 3) col = 6;
            if(i === 4){
                x++;
                y--;
                col = 5;
            }
        }

        // Center the tile
        coords.forEach((coord) => {
            coord.translate(new Coord(0, +4, -4));
        })

        let tiles :Array<Tile> = [];
        for(let i = 0; i < coords.length; i++){
            tiles.push(new Tile(coords[i], MapTiles.getTexture(style)));
        }
        return tiles;
    }

    public static K(style: number = MapTiles.STYLE_G) :Array<Tile>{
        let x   :number = 0;
        let y   :number = 0;
        let z   :number = 0;
        let col :number = 4;
        let row :number = 6;

        let coords :Array<Coord> = [];

        for(let i = 0; i < row; i++){
            for(let j = 0; j < col; j++){
                if(i < 3){
                    coords.push(new Coord(x+j, y-j, z));
                }
                else if(i === 3 && j !== 3){
                    coords.push(new Coord(x+j, y-j, z));
                }
                else if(i === 4 && j !== 3 && j !== 4){
                    coords.push(new Coord(x+j, y-j, z));
                }
                else if(i === 5){
                    if(j === 1 || j === 2 || j === 6 || j === 7){
                        coords.push(new Coord(x+j, y-j, z));
                    }
                }
            }
            x--;
            z++;
            col++;
        }

        // Center the tile
        coords.forEach((coord) => {
            coord.translate(new Coord(0, +3, -3));
        })

        let tiles :Array<Tile> = [];
        for(let i = 0; i < coords.length; i++){
            tiles.push(new Tile(coords[i], MapTiles.getTexture(style)));
        }
        return tiles;
    }

    public static L(style: number = MapTiles.STYLE_G) :Array<Tile>{
        let x   :number = 0;
        let y   :number = 0;
        let z   :number = 0;
        let col :number = 5;
        let row :number = 7;

        let coords :Array<Coord> = [];

        for(let i = 0; i < row; i++){
            for(let j = 0; j < col; j++){
                coords.push(new Coord(x+j, y-j, z));
            }
            if(i%2 === 0){
                y--;
                col = 4;
            }
            else{
                x--;
                col = 5;
            }
            z++;
        }

        // Center the tile
        coords.forEach((coord) => {
            coord.translate(new Coord(0, +3, -3));
        })

        let tiles :Array<Tile> = [];
        for(let i = 0; i < coords.length; i++){
            tiles.push(new Tile(coords[i], MapTiles.getTexture(style)));
        }
        return tiles;
    }

    public static M(style: number = MapTiles.STYLE_G) :Array<Tile>{
        let x   :number = 0;
        let y   :number = 0;
        let z   :number = 0;
        let col :number = 4;
        let row :number = 7;

        let coords :Array<Coord> = [];

        for(let i = 0; i < row; i++){
            for(let j = 0; j < col; j++){
                coords.push(new Coord(x+j, y-j, z));
            }
            if(i < 2){
                x--;
                col++;
            }
            else if (i === 2){
                y--;
                col = 5;
            }
            else if (i === 3){
                x--;
                col = 6;
            }
            else{
                y--;
                col--;
            }
            z++;
        }

        // Center the tile
        coords.forEach((coord) => {
            coord.translate(new Coord(0, +3, -3));
        })

        let tiles :Array<Tile> = [];
        for(let i = 0; i < coords.length; i++){
            tiles.push(new Tile(coords[i], MapTiles.getTexture(style)));
        }
        return tiles;
    }

    public static N(style: number = MapTiles.STYLE_G) :Array<Tile>{
        let x   :number = 0;
        let y   :number = 0;
        let z   :number = 0;
        let col :number = 8;
        let row :number = 7;

        let coords :Array<Coord> = [];

        for(let i = 0; i < row; i++){
            for(let j = 0; j < col; j++){
                coords.push(new Coord(x+j, y-j, z));
            }
            if(i%2 === 0){
                y--;
                col = 7;
            }
            else{
                x--;
                col = 8;
            }
            z++;
        }

        // Center the tile.
        coords.forEach((coord) => {
            coord.translate(new Coord(-2, +5, -3));
        })

        let tiles :Array<Tile> = [];
        for(let i = 0; i < coords.length; i++){
            tiles.push(new Tile(coords[i], MapTiles.getTexture(style)));
        }
        return tiles;
    }

    public static getTexture(style: number) :Array<string>{
        let texture  :string = "";
        let rotation :string = "";
        switch(style){
            case MapTiles.STYLE_G:
                texture  = 'tile-g'+Math.round(Math.random() * 5);
                rotation = 'tile-rotate'+Math.round(Math.random() * 5);
                return [texture, rotation];
            case MapTiles.STYLE_W:
                texture  = 'tile-w0';
                rotation = 'tile-rotate0';
                return [texture, rotation];
            case MapTiles.STYLE_MMS:
                texture  = 'tile-mms0';
                rotation = 'tile-rotate0';
                return [texture, rotation];
            case MapTiles.STYLE_NSB:
                texture  = 'tile-nsb0';
                rotation = 'tile-rotate0';
                return [texture, rotation];
            default:
                return ['tile-background'];
        }
    }
}