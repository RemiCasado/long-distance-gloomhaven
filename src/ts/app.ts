import { GameLevelEditor } from "./modules/game-level-editor/gameLevelEditor";

export class GloomHaven {

    public static LOG_SCREEN        :number = 0;
    public static GAME_LEVEL_EDITOR :number = 1;

    private gameLevelEditor :GameLevelEditor = new GameLevelEditor();    // Obvious
    private _state          :number          = GloomHaven.LOG_SCREEN;    // State of the game (eg. LOG_SCREEN)


    start(state :number = GloomHaven.LOG_SCREEN) {
        switch(state){
            case GloomHaven.LOG_SCREEN:
                console.log('Welcome to GloomHaven !');
                break;
            case GloomHaven.GAME_LEVEL_EDITOR:
                this._startGameLevelEditor();
                break;
        }
        
    }

    private _startGameLevelEditor() :void{
        this._state = GloomHaven.GAME_LEVEL_EDITOR;
        this.gameLevelEditor.display();
    }

}