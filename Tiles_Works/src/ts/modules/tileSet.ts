import { Coord } from "./coord";
import { Tile } from "./tile";

export class TileSet {

    public static copy(tileset: Array<object>){
        let new_tileset = [];
        for(let i = 0 ; i < tileset.length; i++){
            let coord = tileset[i]['c'];
            let tile  = tileset[i]['t'];
            new_tileset.push({
                c: Coord.copy(coord),
                t: Tile.copy(tile)
            })
        }
        return new_tileset;
    }

    public static l1A(): Array<object>{
        return [
            {c: new Coord( 0, 0, 0), t: new Tile()},
            {c: new Coord(-1, 1, 0), t: new Tile()},
            {c: new Coord(-1, 0, 1), t: new Tile()},
            {c: new Coord( 0, 1,-1), t: new Tile()},
            {c: new Coord( 0,-1, 1), t: new Tile()},
            {c: new Coord( 1, 0,-1), t: new Tile()},
            {c: new Coord( 1,-1, 0), t: new Tile()}
        ];
    }
}