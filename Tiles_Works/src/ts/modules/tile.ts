import {Point}  from "./point";
import {Bounds} from "./bounds";

export class Tile {

    public element:  HTMLElement = null;

    private bounds:   Bounds = null;
    private center: Point = null;
    private imageURL: string = null;
    private size:     number = null;  

    constructor(s: number = 0, p: Point = new Point(0, 0)) {
        this.size   = s;
        this.center = p;
        this.element = document.createElement('div');
        this.element.classList.add('tile');
        this.updateElement();
    }

    public static copy(t: Tile){
        return new Tile(t.size, t.center);
    }

    public getBounds(): Bounds {
        return this.bounds;
    }

    public setCenter(p: Point): void {
        this.center = p;
    }

    public setSize(s: number): void{
        this.size = s;
    }

    public updateElement(): void {
        this.bounds = new Bounds(this.size * 2, this.size * Math.sqrt(3));
        this.element.style.height   = this.bounds.CSSHeight;
        this.element.style.width    = this.bounds.CSSWidth;
        this.element.style.top      = (this.center.y - this.bounds.h).toString() + 'px';
        this.element.style.left     = (this.center.x - this.bounds.w).toString() + 'px';
    }
    
}