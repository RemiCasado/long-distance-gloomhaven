import {Point} from "./point";
import {Bounds}   from "./bounds";
import {Tile}     from "./tile";
import {Coord} from "./coord";
import {TileSet} from "./tileSet";

export class Grid {

    public width:        number        = 0;
    public height:       number        = 0;
    public size:         number        = 0;
    public element:      HTMLElement   = null;
    public hoverTileset: Array<object> = null;
    public radius:       number        = 50;
    
    private center:               Point   = null;
    private hoverTilesetBool:     boolean = false;
    private hoverTilesetCoord:    Coord   = null;
    private tiles:                any     = [[],[]];

    constructor(w: number = 0, h: number = 0, s: number = 0, elem: HTMLElement) {
        this.element = elem;
        this.width   = w;
        this.height  = h;
        this.size    = s;
        this.center  = new Point(this.width / 2, this.height / 2);

        document.addEventListener('mousemove', this._onMouseMove.bind(this)); 
        
        // Init the tiles "cubic" matrix.
        for(let i = 0; i < this.radius; i++){
            this.tiles[0].push([[], []]);
            this.tiles[1].push([[], []]);
            for(let j = 0; j< this.radius; j++){
                this.tiles[0][i][0].push([[], []]);
                this.tiles[0][i][1].push([[], []]);
                this.tiles[1][i][0].push([[], []]);
                this.tiles[1][i][1].push([[], []]);
                for(let k = 0; k< this.radius; k++){
                    this.tiles[0][i][0][j][0].push(null);
                    this.tiles[0][i][0][j][1].push(null);
                    this.tiles[0][i][1][j][0].push(null);
                    this.tiles[0][i][1][j][1].push(null);
                    this.tiles[1][i][0][j][0].push(null);
                    this.tiles[1][i][0][j][1].push(null);
                    this.tiles[1][i][1][j][0].push(null);
                    this.tiles[1][i][1][j][1].push(null);
                }
            }
        }
    }

    public addTile(coord: Coord, tile: Tile): void{
        if(tile === undefined || tile === null) throw "Tile is not set."; //TODO : add more details.

        tile.setSize(this.size);
        tile.setCenter(coord.toPoint(this.size, this.center));
        tile.updateElement();

        this.element.appendChild(tile.element);

        //TODO : virer après
        tile.element.innerHTML = coord.x.toString() + ', ' + coord.y.toString() + ', ' + coord.z.toString(); 

        this._addTile(coord, tile);
    }

    private _addTile(coord: Coord, tile: Tile): void{
        let x = null; (coord.x >= 0) ? x = this.tiles[0][coord.x] : x = this.tiles[1][-coord.x];
        let y = null; (coord.y >= 0) ? y = x[0][coord.y] : y = x[1][-coord.y];
        (coord.z >= 0) ? y[0][coord.z] = tile : y[1][-coord.z] = tile;
    }

    public addTileSet(tileset: Array<object>, center: Coord): Array<object>{
        for(let i = 0; i < tileset.length; i++){
            tileset[i]['c'] = Coord.getRelativeCoord(center, tileset[i]['c']);
            this.addTile(tileset[i]['c'], tileset[i]['t']);
        }
        return tileset;
    }

    public getTile(coord: Coord): Tile{
        let x = null; (coord.x >= 0) ? x = this.tiles[0][coord.x] : x = this.tiles[1][-coord.x];
        let y = null; (coord.y >= 0) ? y = x[0][coord.y] : y = x[1][-coord.y];
        return (coord.z >= 0) ? y[0][coord.z] : y[1][-coord.z];
    }

    public removeTileSet(tileset: Array<object>, center: Coord = null){
        for(let i = 0; i < tileset.length; i++){
            let coord = tileset[i]['c'];
            if(center !== null) coord = Coord.getRelativeCoord(center, coord);
            let tile = this.getTile(coord);
            if(tile !== null){
                this.element.removeChild(tile.element);
                this._addTile(coord, null);
            }
        }
        return tileset;
    }

    public moveTileSet(tileset: Array<object>, target: Coord, source: Coord = null): Array<object>{
        tileset = this.removeTileSet(tileset, source);
        return this.addTileSet(tileset, target);
    }

    public startHoverTileset(tileset: Array<object>): void{
        this.hoverTileset     = tileset;
        this.hoverTilesetBool = true;    
    }

    public stopHoverTileset(): void{
        this.hoverTilesetBool = false; 
        this.removeTileSet(this.hoverTileset);
    }

    private _onMouseMove(evt): void{
        if(this.hoverTilesetBool){
            let x = evt.clientX;
            let y = evt.clientY;

            // Calculate target coord.
            let mouse_coord = new Coord();
            mouse_coord.fromPoint(new Point(x, y), this.center, this.size);

            // Move if necessary.
            if(!mouse_coord.isEqual(this.hoverTilesetCoord)){
                let hover_tileset = TileSet.copy(this.hoverTileset);
                this.moveTileSet(hover_tileset, mouse_coord, this.hoverTilesetCoord);
                this.hoverTilesetCoord = mouse_coord;
            }
        }
    }


}