
import {Point}  from "./modules/point";
import {Tile}   from "./modules/tile";
import {Bounds} from "./modules/bounds";
import {Grid}   from "./modules/grid";
import { TileSet } from "./modules/tileSet";
import { Coord } from "./modules/coord";

export class App {
    start() {
        
        let grid = new Grid(1000, 1000, 50, document.getElementById("container"));
        let tileset1 = TileSet.l1A();
        tileset1 = grid.addTileSet(tileset1, new Coord(0,0,0));

        let tileset2 = TileSet.l1A();
        // tileset2 = grid.addTileSet(tileset2, new Coord(3,0,-3));

        grid.startHoverTileset(tileset2);

        setTimeout(() => {
            grid.stopHoverTileset();
        }, 20000);
    }
}