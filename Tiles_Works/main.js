// main.js
  
requirejs.config({
    baseUrl: 'lib',
    paths: {
        'App':'./app',
    }
});

requirejs(['App'], function(MyApp) {
    console.log('starting application...');

    var app = new MyApp.App();
    app.start();
});